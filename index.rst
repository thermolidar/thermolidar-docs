.. Deliverable_2.3 documentation master file, created by
   sphinx-quickstart on Mon May 20 14:25:20 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ThermoLiDAR Plugin
==================

.. sidebar:: Summary

    :Release: |release|
    :Date: |today|
    :Authors: **Roberto Antolin**; **FJ Romero**,  
    :Target: users
    :status: mature

.. toctree::
   :maxdepth: 2
   :numbered:

   Introduction <source/intro>
   Installation <source/installation>
   User Manual <source/manual/index>
   Examples and Tutorials <source/tutorials_examples/index>

.. toctree::
   :maxdepth: 1

   source/helpfiles/index

.. toctree::
   :hidden:

   source/files/shfu_eq



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


