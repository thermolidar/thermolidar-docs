==============
 Installation
==============

Requirements
============

Hardware Requirements
---------------------

Minimum hardware requirements implies

* x86–64 or compatible CPU
* 1 GB RAM
* 20 GB available hard disk space

For a fully operational system, the recommended hardware requirements are:

* x86–64 or compatible CPU
* 4 GB RAM
* 50 GB available hard disk space

Software Requirements
---------------------

THERMOLIDAR software has been developed as a QGIS plug-in and, once installed, it becomes part of the Processing Toolbox of QGIS. This makes possible that final users get free access to lots of GIS capabilities, but also implies that the Processing Toolbox is already installed in QGIS. The QGIS version currently supported by the THERMOLIDAR plug-in is `QGIS 2.4.0 Chugiak <http://www.qgis.org/es/site/forusers/download.html>`_.

For the THERMOLIDAR software to run, the plug-in requires some external libraries that need to be previously installed:

* R_
* SPDLib_

**R** is a free programming language and software environment for statistical computing and is used by the Data Analysis Modules. SPDLib is a set of open source software tools to process LiDAR data and is used by the Data Processing modules.

Another optional software is Fusion_, which is a software for manipulation and analysis of LiDAR data. 

Windows Installer
=================

There exists a THERMOLIDAR installer for Windows versions. The installer helps the user through the process of install **QGIS**, **R**, **SPDLib** and **Fusion** by launching their respectively installers sequentially. In case the user had already installed any package, it would be possible to skip its installation select the software to be installed at the beginning of the process.

For each package, a new *Welcome* window will be popped-up in order to install the package.

.. figure:: ../images/installer/ThermoLiDAR_installer.png
   :width: 400px
   :figwidth: 500px
   :align: center

   THERMOLIDAR installer "Welcome" window

The user will be always questioned about the installation folder. In addition to the path folder, the user can select between different configuration options during the installation of QGIS and R.

.. figure:: ../images/installer/R_installer_options.png
   :width: 400px
   :figwidth: 500px
   :align: center

   R installer windows

Once the installation of each package is complete and its "Good-bye" windows is closed, the new installer will pop-up.

.. figure:: ../images/installer/ThermoLiDAR_installer_Goodbye.png
   :width: 400px
   :figwidth: 500px
   :align: center

   THERMOLIDAR installer final window


Installing SPDlib
=================

Windows
-------

To install SPDLib in Windows systems, download the window binaries that are available in the `SPDLib repository <https://bitbucket.org/petebunting/spdlib/downloads>`_, and unzip them into any folder in the file system. **Be sure the installation path does not contain spaces**. A good place where to install SPDLib is ``C:\SPDLib>`` 


Mac OSX, Linux and Solaris
--------------------------

The notes below provide some useful details on the process for installing SPDLib. These notes are intended for people compiling the software on a UNIX platform such as Mac OSX, Linux or Solaris (these are the platforms on which the software has been tested). 

To compile the software (and the pre-requisites) you will need a C++ compiler, we use the `GNU GCC <http://gcc.gnu.org>`_ compilers but the software has also been tested and compiles without a problem using the SunPro compiler on Solaris and the Intel x86 compilers.

You will also need to have mercurial_ installed to download the latest version of the SPDLib source code, cmake_ to configure the source code before compilation.

Getting the SPDlib Source Code
``````````````````````````````

The SPDLib source code is hosted within a Mercurial repository on bitbucket_. To clone the source code into a folder spdlib run the following command::

	hg clone https://bitbucket.org/petebunting/spdlib spdlib

Compiling SPDlib
````````````````

If libraries are not installed within ``/usr/local`` then the path needs to be specified using the variables available on CMake listed below.

.. code-block:: bash

	$ cmake -D CMAKE_INSTALL_PREFIX=/usr/local \
		-D HDF5_INCLUDE_DIR=/usr/local/include \
		-D HDF5_LIB_PATH=/usr/local/lib \
		-D LIBLAS_INCLUDE_DIR=/usr/local/include \
		-D LIBLAS_LIB_PATH=/usr/local/lib\
		-D GSL_INCLUDE_DIR=/usr/local/include \
		-D GSL_LIB_PATH=/usr/local/lib \
		-D CGAL_INCLUDE_DIR=/usr/local/include \
		-D CGAL_LIB_PATH=/usr/local/lib \
		-D BOOST_INCLUDE_DIR=/usr/local/include \
		-D BOOST_LIB_PATH=/usr/local/lib \
		-D GDAL_INCLUDE_DIR=/usr/local/include \
		-D GDAL_LIB_PATH=/usr/local/lib \
		-D XERCESC_INCLUDE_DIR=/usr/local/include \
		-D XERCESC_LIB_PATH=/usr/local/lib \
		-D GMP_INCLUDE_DIR=/usr/local/include \
		-D GMP_LIB_PATH=/usr/local/lib \
		-D MPFR_INCLUDE_DIR=/usr/local/include \
		-D MPFR_LIB_PATH=/usr/local/lib \
		-D CMAKE_VERBOSE_MAKEFILE=ON \

	$ make
	$ make install

Pre-requisites
``````````````

The SPDLib software library has a number of software prerequisites, which are required to built the software.

During the development process, to date, the following libraries have been included:

#. Boost (http://www.boost.org) (oldest Version 1.49)
#. HDF5 (http://www.hdfgroup.org) (oldest Version 1.8.2)
#. GNU Scientific Library (GSL; http://www.gnu.org/software/gsl) (oldest Version 1.14)
#. Xerces-C (http://xerces.apache.org/xerces-c) (oldest Version 3.1.1)
#. GDAL/OGR (http://www.gdal.org) (oldest Version 1.7)
#. LibLAS (http://www.liblas.org) (oldest Version 1.6)
#. CGAL (http://www.cgal.org) (oldest Version 3.8)

Installing and Configuring ThermoLiDAR software
===============================================

Installation
------------
The easiest way to install ThermoLiDAR plug-in is through the windows installer (see `Windows Installer`_). However, the installer might not be updated or the user may work under Linux or MacOS platforms. If this should be the casegit , the source code of the ThermoLiDAR software can be downloaded from the official and private bitbucket_ repository (`https://bitbucket.org/thermolidar/thermolidar/downloads <https://bitbucket.org/thermolidar/thermolidar/downloads>`_). Once the file is downloaded just unzip it into your system QGIS plug-ins folder. This plug-ins folder should be located in::
	
	%HOMEDRIVE\%HOMEPATH\.qgis2\python\plugins

in Windows systems and, for Unix-based systems::

	~/.qgis2/python/plugins

Finally, the unzip folder has to be renamed to *thermolidar*.

Alternatively, the latest version of the software can be directly clone from the repository to our local QGIS plug-in folder using git_:

.. code-block:: bash

	$ git clone http://bitbucket.org/thermolidar/thermolidar.git ~/.qgis2/python/plugins/thermolidar


Configuration
-------------
Start QGIS and make sure the *Processing Toolbox* is enable and the *Advanced interface* is selected (at the bottom). The Processing Toolbox should look like this:

.. figure:: ../images/thld_toolbox/Window_ProcessingToolBox.png
   :width: 300px
   :figwidth: 500px
   :align: center

   Processing Toolbox


*Manage and Install Plugins* is placed into the *Plugins* menu in QGIS

Now the plugin must be enabled in the Installed tab within the *Plugins > Manager and Install Plugin* menu.

From the *Manage and Install Plugins...* within the *Plugins* menu, select the **ThermoLiDAR** plug-in:

.. figure:: ../images/thld_toolbox/Window_ThermoLiDAR_Plugin.png
   :width: 650px
   :figwidth: 650px
   :align: center

   ThermoLiDAR plug-in is enabled in the *Installed* tab within the *Manage and Install Plugins...*

Once QGIS load ThermoLiDAR, it appears within the Processing Toolbox

.. figure:: ../images/thld_toolbox/Window_ProcessingToolBox_ThermoLiDAR.png
   :width: 300px
   :figwidth: 500px
   :align: center

   ThermoLiDAR fully integrated into Processing Toolbox

However, to use the plug-in SPDLib and R software have to be enabled and visible from QGIS. Otherwise the user will get an error message that denies running any tool:

.. WARNING::
	SPDTools folder is not configured. Please, consider to configure it before running SPDTools algorithms.

To activate them, open the *Processing > Options and configuration > Providers*  menu. First, enable the *R statistical package* and provide the *R Scripts Folder* and the *R folder*. *R Scripts Folder* specifies where the R scripts are located, ``C:\Users\admin\.qgis2\processing\scripts``, and *R folder* specifies where R is installed, ``C:\Program Files\R\R-XXX`` (*XXX* stands for the current R version).

Finally, activate the enable the ThermoLiDAR plug-in and supplies the folder of the SPDLib binaries, commonly ``C:\SPDLib>`` in Windows systems and ``/usr/local/bin``

.. figure:: ../images/thld_toolbox/Window_OptAndConfig_SPDLib_R.png
   :width: 350px
   :figwidth: 500px
   :align: center

   Visualization of *Processing > Options and configuration* window


.. _mercurial: http://mercurial.selenic.com
.. _cmake: http://www.cmake.org
.. _bitbucket: https://bitbucket.org/petebunting/spdlib
.. _git: http://git-scm.com/
.. _R: http://www.r-project.org/
.. _SPDLib: http://www.spdlib.org/doku.php
.. _Fusion: http://forsys.cfr.washington.edu/fusion/fusionlatest.html