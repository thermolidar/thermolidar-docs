**********
spdmetrics
**********

SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdmetrics  {--image|--vector|--ascii}  [-v <String>] -m <String> -o  <String> -i <String> [-f <string>]  [-b <float>] [-c <unsigned int>]  [-r <unsigned int>] [--]  [--version] [-h]``


Where
-----
--image  (OR required)  Run metrics with image output

**-- or --**
--vector  (OR required)  Run metrics with vector output

**-- or --**
--ascii  (OR required)  Run metrics with ASCII output

-v <String>, --vectorfile <String>  The input vector file.
-m <String>, --metricsxml <String>  (required)  The output SPD file.
-o <String>, --output <String>  (required)  The output file.
-i <String>, --input <String>  (required)  The input SPD file.
-f <string>, --format <string>  Image format (GDAL driver string), Default is ENVI.
-b <float>, --binsize <float>  Bin size for processing and output image (Default 0) - Note 0 will use  the native SPD file bin size.
-c <unsigned int>, --blockcols <unsigned int>  Number of columns within a block (Default 0) - Note values greater  than 1 result in a non-sequencial SPD file.
-r <unsigned int>, --blockrows <unsigned int>  Number of rows within a block (Default 100)
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Calculate metrics : **spdmetrics**