*********
spdsubset
*********

SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdsubset  -o <String> -i <String> [--num  <uint_fast32_t>] [--start  <uint_fast32_t>] [--shpfile  <string>] [--ignorez]  [--ignorerange] [--txtfile  <string>] [--spherical] [--height]  [--ranmax <double>] [--ranmin  <double>] [--zenmax <double>]  [--zenmin <double>] [--azmax  <double>] [--azmin <double>]  [--hmax <double>] [--hmin <double>]  [--zmax <double>] [--zmin <double>]  [--ymax <double>] [--ymin <double>]  [--xmax <double>] [--xmin <double>]  [--] [--version] [-h]``


Where
-----
-o <String>, --output <String>  (required)  The output SPD file.
-i <String>, --input <String>  (required)  The input SPD file.
--num <uint_fast32_t>  Number of pulses to be exported
--start <uint_fast32_t>  First pulse in the block
--shpfile <string>  A shapefile to which the dataset should be subsetted to
--ignorez  Defining that Z should be ignored when subsetting using a text file.
--ignorerange  Defining that range should be ignored when subsetting using a text  file.
--txtfile <string>  A text containing the extent to which the file should be cut to.
--spherical  Subset a spherically indexed SPD file.
--height  Threshold the height of each pulse (currently only valid with SPD to  SPD subsetting)
--ranmax <double>  Maximum range threshold
--ranmin <double>  Minimum range threshold
--zenmax <double>  Maximum zenith threshold
--zenmin <double>  Minimum zenith threshold
--azmax <double>  Maximum azmuth threshold
--azmin <double>  Minimum azimuth threshold
--hmax <double>  Maximum Height threshold
--hmin <double>  Minimum Height threshold
--zmax <double>  Maximum Z threshold
--zmin <double>  Minimum Z threshold
--ymax <double>  Maximum Y threshold
--ymin <double>  Minimum Y threshold
--xmax <double>  Maximum X threshold
--xmin <double>  Minimum X threshold
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Subset point cloud data: **spdsubset**