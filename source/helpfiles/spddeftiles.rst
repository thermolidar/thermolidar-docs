***********
spddeftiles
***********

SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spddeftiles  {-t|-e} [-i <String>] [-o  <String>] [--ymax <double>] [--xmax  <double>] [--ymin <double>] [--xmin  <double>] [--overlap <double>]  [--ysize <double>] [--xsize  <double>] [--] [--version] [-h]``


Where
-----
-t, --tiles  (OR required)  Define a set of tiles for a region.

**-- or --**
-e, --extent  (OR required)  Calculate the extent of a set of files.

-i <String>, --input <String>  Input file listing the set of input files (--extent).
-o <String>, --output <String>  Output XML file defining the tiles (--tiles).
--ymax <double>  Y max (in units of coordinate systems) of the region to be tiled  (--tiles).
--xmax <double>  X max (in units of coordinate systems) of the region to be tiled  (--tiles).
--ymin <double>  Y min (in units of coordinate systems) of the region to be tiled  (--tiles).
--xmin <double>  X min (in units of coordinate systems) of the region to be tiled  (--tiles).
--overlap <double>  Size (in units of coordinate systems) of the overlap for tiles  (Default 100) (--tiles).
--ysize <double>  Y size (in units of coordinate systems) of the tiles (Default 1000)  (--tiles).
--xsize <double>  X size (in units of coordinate systems) of the tiles (Default 1000)  (--tiles).
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Tools for defining a set of tiles: **spddeftiles**