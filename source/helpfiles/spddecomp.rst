*********
spddecomp
*********

SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spddecomp  -o <String> -i <String> [-a]  [-d <float>] [-w <uint_fast32_t>]  [-e <uint_fast32_t>] [-n] [-t  <uint_fast32_t>] [-c <unsigned  int>] [-r <unsigned int>] [--]  [--version] [-h]``


Where
-----
-o <String>, --output <String>  (required)  The output file.
-i <String>, --input <String>  (required)  The input file.
-a, --all  Fit all Gaussian at once
-d <float>, --decay <float>  Decay value for ignoring ringing artifacts (Default 5)
-w <uint_fast32_t>, --window <uint_fast32_t>  Window for the values taken either side of the peak for fitting  (Default 5)
-e <uint_fast32_t>, --decaythres <uint_fast32_t>  Intensity threshold above which a decay function is used (Default 100)
-n, --noise  Estimate noise. Only applicable when --all is set. Note an initial  estimate is required for peak detection (see -t)
-t <uint_fast32_t>, --threshold <uint_fast32_t>  Noise threshold below which peaks are ignored (Default: Value in  pulse->waveNoiseThreshold)
-c <unsigned int>, --blockcols <unsigned int>  Number of columns within a block (Default 0) - Note values greater  than 1 result in a non-sequencial SPD file.
-r <unsigned int>, --blockrows <unsigned int>  Number of rows within a block (Default 100)
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Decompose full waveform data to create discrete points: **spddecomp**