*********
spddefrgb
*********

SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spddefrgb  {--define|--stretch} -o  <String> [--image <String>] -i  <String> [--independ] [--coef  <float>] [--blue <uint_fast16_t>]  [--green <uint_fast16_t>] [--red  <uint_fast16_t>] [-c <unsigned  int>] [-r <unsigned int>]  [--stddev] [--linear] [--]  [--version] [-h]``


Where
-----
--define  (OR required)  Define the RGB values on an SPD file from an input  image.

**-- or --**
--stretch  (OR required)  Stretch existing RGB values to a range of 0 to 255.

-o <String>, --output <String>  (required)  The output SPD file.
--image <String>  The input image file.
-i <String>, --input <String>  (required)  The input SPD file.
--independ  Stretch the RGB values independently.
--coef <float>  The coefficient for the standard deviation stretch (Default is 2)
--blue <uint_fast16_t>  Image band for blue channel
--green <uint_fast16_t>  Image band for green channel
--red <uint_fast16_t>  Image band for red channel
-c <unsigned int>, --blockcols <unsigned int>  Number of columns within a block (Default 0) - Note values greater  than 1 result in a non-sequencial SPD file.
-r <unsigned int>, --blockrows <unsigned int>  Number of rows within a block (Default 100)
--stddev  Use a linear 2 standard deviation stretch.
--linear  Use a linear stretch between the min and max values.
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Define the RGB values on the SPDFile: **spddefrgb**