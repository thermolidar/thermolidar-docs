**********
spdoverlap
**********

SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdoverlap  {-c|-s} [-o <String>] [--]  [--version] [-h] <string> ...``


Where
-----
-c, --cartesian  (OR required)  Find cartesian overlap.

**-- or --**
-s, --spherical  (OR required)  Find spherical overlap.

-o <String>, --output <String>  The output file.
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.

``<string>``
    (accepted multiple times)  (required)  File names for the output (if required) and input files

NAME
====
Calculate the overlap between UPD and SPD files: **spdoverlap**