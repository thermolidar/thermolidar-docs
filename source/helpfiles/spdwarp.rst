*******
spdwarp
*******

SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdwarp  {--shift|--warp} -o <String> -i  <String> [-c <unsigned int>] [-r  <unsigned int>] [-y <float>] [-x  <float>] [-g <std::string>]  [--order <unsigned int>] [-p <>]  [-t <POLYNOMIAL|NEAREST_NEIGHBOR|TRIANGULATION>] [--] [--version]  [-h]``


Where
-----
--shift  (OR required)  Apply a linear shift to the SPD file.

**-- or --**
--warp  (OR required)  Apply a nonlinear warp to the SPD file defined by a set  of GCPs.

-o <String>, --output <String>  (required)  The output SPD file.
-i <String>, --input <String>  (required)  The input SPD file.
-c <unsigned int>, --blockcols <unsigned int>  Number of columns within a block (Default 0) - Note values greater  than 1 result in a non-sequencial SPD file.
-r <unsigned int>, --blockrows <unsigned int>  Number of rows within a block (Default 100)
-y <float>, --yshift <float>  SHIFT: The y shift in the units of the dataset (probably metres).
-x <float>, --xshift <float>  SHIFT: The x shift in the units of the dataset (probably metres).
-g <std::string>, --gcps <std::string>  WARP: The path and file name of the gcps file.
--order <unsigned int>  POLY TRANSFORM (Default=3): The order of the polynomial fitted.
-p <>, --pulsewarp <>  WARP (Default=PULSE_IDX): The eastings and northings used to calculate  the warp. ALL_RETURNS recalculates the offsets for each X,Y while  PULSE_IDX and PULSE_ORIGIN use a single offset for the whole pulse.
-t <POLYNOMIAL|NEAREST_NEIGHBOR|TRIANGULATION>, --transform <POLYNOMIAL|NEAREST_NEIGHBOR|TRIANGULATION>  WARP (Default=POLYNOMIAL): The transformation model to be fitted to  the GPCs and used to warp the data.
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Interpolate a raster elevation surface: **spdwarp**