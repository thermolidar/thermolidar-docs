*********
spdpffgrd
*********

SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdpffgrd  -o <String> -i <String>  [--gdal <string>] [--class  <uint_fast16_t>] [--morphmin]  [--image] [-m <uint_fast16_t>] [-f  <uint_fast32_t>] [--tophatscales  <bool>] [-t <uint_fast32_t>] [-s  <uint_fast32_t>] [-k  <uint_fast32_t>] [--grd <float>]  [--overlap <uint_fast16_t>] [-b  <float>] [-c <unsigned int>] [-r  <unsigned int>] [--] [--version]  [-h]``


Where
-----
-o <String>, --output <String>  (required)  The output file.
-i <String>, --input <String>  (required)  The input SPD file.
--gdal <string>  Provide the GDAL driver format (Default ENVI), Erdas Imagine is HFA,  KEA is KEA
--class <uint_fast16_t>  Only use points of particular class
--morphmin  Apply morphological opening and closing to remove multiple path  returns (note this can remove real ground returns).
--image  If set an image of the output surface will be generated rather than  classifying the points (useful for debugging and parameter selection)
-m <uint_fast16_t>, --mpd <uint_fast16_t>  Minimum point density in block to use for surface estimation - default  40
-f <uint_fast32_t>, --tophatfactor <uint_fast32_t>  How quickly the tophat window reduces through the resolution, higher  numbers reduce size quicker - default 2
--tophatscales <bool>  Whether the tophat window size decreases through the resolutions -  default true
-t <uint_fast32_t>, --tophatstart <uint_fast32_t>  Starting window size (actually second, first is always 1) for tophat  transforms, must be >= 2, setting this too big can cause segfault! -  default 4
-s <uint_fast32_t>, --stddev <uint_fast32_t>  Number of standard deviations used in classification threshold -  default 3
-k <uint_fast32_t>, --kvalue <uint_fast32_t>  Number of stddevs used for control point filtering - default 3
--grd <float>  Threshold for deviation from identified ground surface for classifying  the ground returns (Default 0.3)
--overlap <uint_fast16_t>  Size (in bins) of the overlap between processing blocks (Default 10)
-b <float>, --binsize <float>  Bin size for processing and output image (Default 0) - Note 0 will use  the native SPD file bin size.
-c <unsigned int>, --blockcols <unsigned int>  Number of columns within a block (Default 0) - Note values greater  than 1 result in a non-sequencial SPD file.
-r <unsigned int>, --blockrows <unsigned int>  Number of rows within a block (Default 100)
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Classifies the ground returns using a parameter-free filtering  algorithm: **spdpffgrd**