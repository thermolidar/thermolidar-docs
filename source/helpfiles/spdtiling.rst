*********
spdtiling
*********

SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdtiling  {--all|--extract|--extractcore|--tilespdfile|--builddirs|--rmdirs|--upxml|--xml2shp} [--deleteshp]  [-u] [-d] [--useprefix]  [--usedirstruct] [-c <Unsigned  int>] [-r <Unsigned int>] [--wkt  <String>] [-i <String>] [-o  <String>] -t <String> [--]  [--version] [-h]``


Where
-----
--all  (OR required)  Create all tiles.

**-- or --**
--extract  (OR required)  Extract an individual tile as specified in the XML  file.

**-- or --**
--extractcore  (OR required)  Extract the core of a tile as specified in the XML  file.

**-- or --**
--tilespdfile  (OR required)  Tile an input SPD file the core of a tile as specified  in the XML file.

**-- or --**
--builddirs  (OR required)  Creates a directory structure (rowXX/colXX/tiles) for  tiles specified in the XML file.

**-- or --**
--rmdirs  (OR required)  Removes the directories within the structure  (rowXX/colXX/tiles) which do not have any tiles.

**-- or --**
--upxml  (OR required)  Using a list of input files this option updates the XML  file to only contain tiles with exist.

**-- or --**
--xml2shp  (OR required)  Create a polygon shapefile for the tiles within the XML  file.

--deleteshp  If shapefile exists delete it and then run.
-u, --updatexml  Update the tiles XML file.
-d, --deltiles  Remove tiles which have no data.
--useprefix  Use a prefix of the input file name within the output tile name (Only  available for --tilespdfile).
--usedirstruct  Use the prebuild directory structure in the output base path for  outputs (Only available for --tilespdfile).
-c <Unsigned int>, --col <Unsigned int>  The column of the tile to be extracted (--extract).
-r <Unsigned int>, --row <Unsigned int>  The row of the tile to be extracted (--extract).
--wkt <String>  A wkt file with the projection of the output shapefile. (--xml2shp  requires wkt file to be provided)
-i <String>, --input <String>  A text file with a list of input files, one per line. (--extractcore  and --tilespdfile expects a single input SPD file, --xml2shp doesn't  require an input)
-o <String>, --output <String>  The base path for the tiles. (--extractcore expects a single output  SPD file, --xml2shp expects a single shapefile, --upxml doesn't have  an output)
-t <String>, --tiles <String>  (required)  XML file defining the tile regions
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Tools for tiling a set of SPD files using predefined tile areas:  **spdtiling**