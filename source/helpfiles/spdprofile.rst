**********
spdprofile
**********
 
SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdprofile  -o <String> -i <String> [-f  <std::string>] [-b <float>] [-c  <unsigned int>] [-r <unsigned int>]  [-n <unsigned int>] [-t <unsigned  int>] [-m <float>] [-w <unsigned  int>] [--order <unsigned int>]  [--smooth] [--] [--version] [-h]``


Where
-----
-o <String>, --output <String>  (required)  The output file.
-i <String>, --input <String>  (required)  The input SPD file.
-f <std::string>, --format <std::string>  Image format (GDAL driver string), Default is ENVI.
-b <float>, --binsize <float>  Bin size for processing and output image (Default 0) - Note 0 will use  the native SPD file bin size.
-c <unsigned int>, --blockcols <unsigned int>  Number of columns within a block (Default 0) - Note values greater  than 1 result in a non-sequencial SPD file.
-r <unsigned int>, --blockrows <unsigned int>  Number of rows within a block (Default 100)
-n <unsigned int>, --numbins <unsigned int>  The number of bins within the profile (Default: 20).
-t <unsigned int>, --topheight <unsigned int>  The highest bin of the profile (Default: 40).
-m <float>, --minheight <float>  The the height below which points are ignored (Default: 0).
-w <unsigned int>, --window <unsigned int>  The window size ((w*2)+1) used for the smoothing filter (Default: 3).
--order <unsigned int>  The order of the polynomial used to smooth the profile (Default: 3).
--smooth  Apply a Savitzky Golay smoothing to the profiles.
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Generate vertical profiles: **spdprofile**