Case Study 1 - Huelva (Spain)
=============================
Thermal Processing
------------------
.. include:: ./example_huelva/thermal_calibration.rst
.. include:: ./example_huelva/tc_ta.rst

Data analysis
-------------
.. include:: ./example_huelva/hcl.rst
.. include:: ./example_huelva/shfu.rst
.. include:: ./example_huelva/fhc.rst


Case Study 2 - Almería (Spain)
==============================
Thermal Processing
------------------
.. include:: ./example_almeria/thermal_calibration.rst
.. include:: ./example_almeria/tc_ta.rst

Data analysis
-------------
.. include:: ./example_almeria/hcl.rst
.. include:: ./example_almeria/shfu.rst
.. include:: ./example_almeria/fhc.rst