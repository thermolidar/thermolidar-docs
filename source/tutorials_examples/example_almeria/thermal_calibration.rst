Thermal Calibration
~~~~~~~~~~~~~~~~~~~

First, we have a thermal image of the study area. Each digital image value represents the temperature in degrees kelvin.
    
.. figure:: example_almeria/images/thermal_img_Filabres.png
   :width: 450px
   :figwidth: 700px
   :align: center
    
   Thermal image of Sierra de los Filabres

Several values of temperature field of invariant surfaces (black and white cloth) have been collected, and GPS position of each sample.

.. figure:: example_almeria/images/thermal_field_data.png
   :width: 450px
   :figwidth: 700px
   :align: center
    
   Attribute table of shapefile containing the field thermal data.

We introduce the input parameters in the user interface:
    
.. figure:: example_almeria/images/iu_thermal_calibration.png
   :width: 450px
   :figwidth: 700px
   :align: center

   Interface of the Thermal Calibration module
    
As a result the software generates a calibrated image of the study area.