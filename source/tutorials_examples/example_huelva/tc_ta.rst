Tc - Ta
~~~~~~~

We proceed to subtract the air temperature to the calibrated raster temperatures. We have available the following information. 

* Raster temperature calibrated in the previous section. **thld_training\\huelva\\Z1_holmOak\\out\\thermal_processing\\t_calibration.tif**
* Air temperature acquired in flight time. In our case we will use the value of temperature **298.15 ºK**

We proceed to perform the calibration of the thermal image through Thermolidar tool toolbox **[Processing] Thermal - Tc-Ta**.

.. figure:: ../../images/thld_toolbox/tc_ta.png
   :width: 285px
   :figwidth: 700px
   :align: center
   
   **Tc Ta** module is located in the “Thermal” submenu of the THERMOLIDAR plugin toolbox 

We introduce the input parameters in the user interface:

.. figure:: example_huelva/images/iu_tc_ta.png
	:align: center
	
	Interface of the Tc-Ta module
	
Obtaining as output file the following result:

.. figure:: example_huelva/images/thermal_tcta_out.png
	:width: 450px
	:figwidth: 700px
	:align: center
	
	Output thermal image of Huelva
