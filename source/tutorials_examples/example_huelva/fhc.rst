Forest Health Classification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We proceed to perform the classification of the thermal image, based on the following information available:

* **thld_training\\huelva\\Z1_holmOak\\Temperature\\t_huelvaZ1.tif**
* **thld_training\\huelva\\Z1_holmOak\\out\\shfu.shp**

For supervised classification we will use health condition levels obtained in previous sections.

* **thld_training\\huelva\\Z1_holmOak\\out\\aois.shp**

Unsupervised pixel-based classification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Through this tool raster classify the temperature as many classes as you specify. Temperature classification is performed based on homogeneous units, so many output raster have been defined as SHFU be obtained. 

In our case, we obtain three raster classified. Each raster has associated many categories defined temperature.

.. figure:: example_huelva/images/fhc_unsupervised_pixel.png
	:align: center
	
	Interface of the “Unsupervised pixel-based classification” module

We get as output:

.. figure:: example_huelva/images/fhc_unsupervised_pixel_out.png
	:align: center
	
.. figure:: example_huelva/images/fhc_unsupervised_pixel_out2.png
	:align: center
	
.. figure:: example_huelva/images/fhc_unsupervised_pixel_out3.png
	:align: center
	
.. figure:: example_huelva/images/fhc_unsupervised_pixel_out4.png
	:align: center
	
Unsupervised object-based classification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the same way that the pixel-oriented, this tool classification raster classify the temperature as many classes as you specify. Temperature classification is performed based on homogeneous units, so many output raster have been defined as SHFU be obtained. 

In our case, we obtain three raster classified. Each raster has associated many categories defined temperature. 

The difference from the previous tool, is that instead of being classified pixels are classified objects. The classification is made based on the mean value of each of the objects.

.. figure:: example_huelva/images/fhc_unsupervised_object.png
	:align: center
	
	Interface of the “Unsupervised object-based classification” module

We get as output:

.. figure:: example_huelva/images/fhc_unsupervised_object_out.png
	:align: center
	
.. figure:: example_huelva/images/fhc_unsupervised_object_out2.png
	:align: center
	
.. figure:: example_huelva/images/fhc_unsupervised_object_out3.png
	:align: center
	
.. figure:: example_huelva/images/fhc_unsupervised_object_out4.png
	:align: center
	
Supervised pixel-based classification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Through this tool the temperature raster will be classified, based on the condition levels defined in the regions of interest (AOIs).
* **thld_training\\huelva\\Z1_holmOak\\out\\aois.shp**

Temperature classification is performed based on homogeneous units, so many output raster have been defined as SHFU be obtained. 

In our case, we obtain three raster classified. Each raster has associated many categories defined temperature.

.. figure:: example_huelva/images/fhc_supervised_pixel.png
	:align: center
	
	Interface of the “Supervised pixel-based classification” module

We get as output:

.. figure:: example_huelva/images/fhc_supervised_pixel_out.png
	:align: center
	
.. figure:: example_huelva/images/fhc_supervised_pixel_out2.png
	:align: center
	
.. figure:: example_huelva/images/fhc_supervised_pixel_out3.png
	:align: center
	
.. figure:: example_huelva/images/fhc_supervised_pixel_out4.png
	:align: center
	
Supervised object-based classification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Through this tool the temperature raster will be classified, based on the condition levels defined in the regions of interest (AOIs).
* **thld_training\\huelva\\Z1_holmOak\\out\\aois.shp**

Temperature classification is performed based on homogeneous units, so many output raster have been defined as SHFU be obtained. 

In our case, we obtain three raster classified. Each raster has associated many categories defined temperature. 

The difference from the previous tool, is that instead of being classified pixels are classified objects. The classification is made based on the mean value of each of the objects.

.. figure:: example_huelva/images/fhc_supervised_object.png
	:align: center
	
	Interface of the “Supervised object-based classification” module

We get as output:

.. figure:: example_huelva/images/fhc_supervised_object_out.png
	:align: center
	
.. figure:: example_huelva/images/fhc_supervised_object_out2.png
	:align: center
	
.. figure:: example_huelva/images/fhc_supervised_object_out3.png
	:align: center
	
.. figure:: example_huelva/images/fhc_supervised_object_out4.png
	:align: center