Forest Models
=============

To measure the field biological and physical properties (e.g. dominant height, mean diameter, stem number, basal area, timber volume, etc...) throughout entire woodlands is impossible. However, due to the characteristics of LiDAR technology it is possible to assess these properties in wide areas. 

Two different models have been implemented in the |thld| plug-in to this purpose. The first one implements an empirical method where field measurements are correlated with LiDAR data. In this case, only a few sample plots are commonly measured in field in order to relate these measurements to canopy height metrics derived from LiDAR data. These relationships are then used to estimate and extend those characteristics to the area covered by LiDAR creating forest inventory cartography. This method is commonly called *Scandinavian method*.

The second is a hybrid method where some equation relating LiDAR canopy height and forest variables have been defined *a priori*. This method does not require in the field measurement and once is calibrated, it can be used anywhere else. 

Besides, ... 

.. warning:: Some introduction to Stomatal conductance module is missing!


Stepwise Multivariate Regression Model
--------------------------------------
The Stepwise Multivariate Regression model was firstly introduce by Naesset (1997a, 1997b) to estimates tree heights ([Naesset1997a]_) and volumes ([Naesset1997b]_). The methodology assumes that stands have been previously classified before sample plots are measured. Plots must have the same size and have to be regularly distributed throughout the study area. Height metrics derived from LiDAR have to be calculated within each single sample plot (see `Generate metrics`_) excluding points lower than 2 meters, so that stones and shrubs are avoided. Metrics have to include (Naesset 2002; [Naesset2002]_):

* Quantiles corresponding to the 0th, 5th, 10th, 15th, ..., 90th, 95th, 98th percentiles of the distribution.
* The maximum values
* The mean values
* The coefficients of variation
* Measures of canopy density

Naesset definition of canopy density is considered as the proportions of the first echo laser hits above 10th, ..., 90th percentiles of the first echo height distribution to total number of first echoes.

For each sample plot a logarithmic regression equation is formulated:

 .. math::
   Y = \beta_0 h_0^{\beta_1} h_{10}^{\beta_2} \ldots h_{90}^{\beta_{11}} h_{max}^{\beta_{12}} h_{mean}^{\beta_{13}} d_{10}^{\beta_{14}} \ldots d_{90}^{\beta_{23}} \ldots

which, in linear form is expressed as:

 .. math::
   \ln Y &= \ln\beta_0 + \beta_1 \ln h_0 + \beta_2 \ln h_{10} \ldots + \beta_{11} \ln h_{90} + \beta_{12} + \\
         &+ \ln h_{max} + \beta_{13} \ln h_{mean} + \beta_{14} \ln d_{10} + \ldots + \beta_{23} \ln d_{90}^{\beta_23} + \ldots

Where :math:`Y` are the field values (**dependent variable**); :math:`h_{i}` are height percentiles; :math:`h_{max}` and :math:`h_{mean}` are maximum and mean height, respectively; and :math:`d_{j}` are Naesset canopy densities.

.. The model is adjusted with a stepwise selection of the **independent variables** (:math:`h_{i}, h_{max}, h_{mean}, d_{j}, \ldots`) approach, where variables are rejected or left in the model depending on its significance level according to a *F* statistic. A report of the adjust and the model obtained is finally output in a html file.

.. figure:: ../../images/thld_toolbox/stepwise_regression.png
   :width: 450px
   :figwidth: 700px
   :align: center

   The module to calibrate the the forest model is located in *[Analysis] Forest Model* submenu of the |thld| toolbox

Parameters
~~~~~~~~~~

.. figure:: ../../images/lidar/Module_multivariate_regression.png
   :width: 450px
   :figwidth: 700px
   :align: center

   Interface to calculate calibrate the forest model

Required Input Parameters
`````````````````````````

* **Layer**: Shapefile layer that contains the LiDAR metrics.
* **Dependent Variable**: Vector field containing the observations of the predictable variable
* **Independent Variable i**: Vector field containing the LiDAR metrics that will be use to characterized the model

Output Parameters
`````````````````
* **Output**: (html) File containing the final report of the multivariate regression


Hybrid model
------------

.. warning:: Juan to write a small theoretical introduction about what the model does 

This module implements a hybrid model to infer some forestry variables. The equations of the model can be written as:

.. math::

   TH = H_c \cdot 1.10

   SI = \alpha_1 \dfrac{TH}{\left[1-\exp(-\alpha_2 \cdot age)\right]^{\alpha_3}}

   Yield = \beta_1 + \beta_2 \cdot SI

   Mean_{DBH} = b_1 \cdot TH^{b_4}

   Trees_{ha} = \exp(c_1 - c_4 \cdot \log(Mean_{DBH}))

   Volume_{ha} = g_1 \cdot TH^{g_3}

   Basal\_Area_{ha} = h_1 \cdot TH^{h_4}

where :math:`TH` is Top Height, :math:`SI` is Site Index, :math:`Yield` is Yield class, :math:`Mean_{DBH}` is Mean DBH, :math:`Trees_{ha}` is the number of trees per hectare, :math:`Volume_{ha}` is the volume of biomass per hectare, :math:`Basal\_Area_{ha}` is the basal area per hectare; and :math:`\left\{\alpha_1, \alpha_2, \alpha_3, \beta_1, \beta_2, b_1, b_4, c_1, c_4, g_1, g_3, h_1, h_4\right\}` depends on the specie. :math:`TH` is well correlated with the :math:`95^{th}` height percentile for *Norway Spruce* and *Sitka Spruce*, and with the :math:`90^{th}` height percentile for *Scot Pine*, depending on the specie.

In case no information about trees age is provided, a singularity occurs in the *Site Index* and *Yield Class* equations, and thus these variables cannot be then computed.

If the study area contains various stands with different species and ages, then users could be interested in using a layer with sub-compartmentation information. In this case, both age and vegetation layers can be provided in order to overwrite the default constant values of **age** and **vegetation**. Layers require to specify the name of the attribute column for each parameter (**Age column** and **Vegetation column**).

The output raster will contain 7 different bands, one per each different estimated variable: **Top Height**, **Mean DBH**, **number of trees** per hectare, **Volume** per hectare, **Basal Area** per hectare, **Site Index** and **Yield Class**. However, when **age** has been set by default, that is equal to 0, only 5 bands are created in the output raster due to the restrictions in the equations mentioned above.


.. figure:: ../../images/thld_toolbox/hybrid.png
   :width: 450px
   :figwidth: 700px
   :align: center

   This figure should depict where to find the module

Parameters
~~~~~~~~~~

.. figure:: ../../images/lidar/Module_hybrid_method.png
   :width: 450px
   :figwidth: 700px
   :align: center

   This figure should show the module graphical interface 

Required Input Parameters
`````````````````````````
* **Canopy**: (*raster*) Canopy height layer 
* **Age**: (*integer*): Age of the vegetation (years) (Default 0)

Optional Input Parameters
`````````````````````````
* **Age layer**: (*vector*) Sub-compartmentation at stand level with age layer
* **Age column**: (*string*) Name of the column attribute for age sub-compartmentation
* **Vegetation layer**: (*vector*) Sub-compartmentation at stand level with species information
* **Vegetation column**: (*string*) Name of the column attribute for species sub-compartmentation
* **Vegetation type**:
   - **Sitka Spruce**
   - **Norway Spruce**
   - **Scots Pine**

Output Parameters
`````````````````

* **Output**: Output raster


Canopy stomatal conductance
---------------------------

The module calculates canopy stomatal conductance (**TC**) of an area based on the information supplied by canopy temperature, terrain, canopy model, LAI, vegetation type and weather conditions amongst others parameters. The model implements the methodology proposed by Blonquist et al., 2009 ([blonquist2009]_). 

The user is required to provide raster layers for **canopy temperature**, **digital terrain model** and **canopy height model** as input parameters, an output raster layer for the **output** and the day of the year (**DOY**). DOY is needed in order to calculate the solar radiation of the particular day and hour of the year when measurements were taken, and must be provided as *DDMMYY:hhmm*, where **DD** is the day of the month, **MM** is the month of the year, **YYYY** is the year, **hh** is the hour of the day (*24h* format) and **mm** is the minutes of the hour. 

**LAI**, **wind speed** and **air temperature** parameters have default values that will be used as constants throughout the whole area. Users can provide different values or even raster layers that to continuously cover the study area. **Latitude**, **precipitation**, **pressure**, **humidity** should be set to meet location and the weather conditions of the thermal survey. Parameters such as *quantum yield efficiency*, *fraction of photosynthetic active radiation*, *light extinction coefficient*, or *atmospheric turbidity factor* are optional and default values can be used in most cases. 

Ground coverage reflects energy in different ways depending on the coverage type. **Vegetation type** can be selected from a list in order to set the corresponding energy reflection coefficient. Types available are:
* Crop: 0.31
* Deciduous low sun: 0.29
* Deciduous high sun: 0.23
* Conifers: 0.312
* Grass: 0.24
* Snow: 0.85

.. warning:: Georgios to write a small theoretical introduction about what the model does (Yes, you can use LaTeX equations :) )


.. figure:: ../../images/thld_toolbox/cs_conductance.png
   :width: 450px
   :figwidth: 700px
   :align: center

   This figure should depict where to find the module

Parameters
~~~~~~~~~~

.. figure:: ../../images/lidar/Module_conductance.png
   :width: 450px
   :figwidth: 700px
   :align: center

   This figure should show the module graphical interface 

Required Input Parameters
`````````````````````````

* **Canopy temperature**: (*raster*) Canopy temperature layer 
* **Digital terrain model**: (*raster*) Digital terrain model layer
* **Canopy hight model**: (*raster*) Canopy hight model layer

Optional Input Parameters
`````````````````````````

* **LAI layer**: (*vector*) Leaf area index layer
* **LAI**: (*float*) Leaf area index constant value (m2/m2) (Default: 3.0)
* **Wind layer**: (*vector*) Wind speed layer
* **Wind**: (*float*) Wind speed (m s-1) (Default: 2.0)
* **Air Temperature layer** Air temperature layer
* **Temperature** (*float*) Air temperature constant value (Default: 0.0)
* **Vegetation type**:
   - **Conifers**
   - **Crop**
   - **Deciduous low sun**
   - **Deciduous high sun**
   - **Grass**
   - **Snow**

* **DOY**: (*string*) Day of the year to calculate sun radiance (Format: DDMMYYYY:hhmm)
* **Latitude**: (*float*) Approximate latitude of study area (degrees)
* **Pressure**: (*float*) Barometric pressure (mb) (Default: 1013.25)
* **Precipitation**: (*float*) Precipitation (mm) (Default: 0.0)
* **Humidity**: (*float*) Relative humidity (%) (Default: 0.00)
* **Wind height**: (*float*) Wind reference height (m) (Default: 0.0)
* **Temperature height**: (*float*) Temperature reference height (m) (Default: 0.0)
* **Yield**: (*float*) Quantum yield efficiency (mol C mol-1 PAR) (Default: 0.05)
* **Active radiation**: (*float*) Fraction of photosynthetic active radiation (Default: 0.45)
* **Respiration**: (*float*) Fraction of respiration (Default: 0.5)
* **Light extinction coefficient**: (*float*) Light extinction coefficient (Default: 0.5)
* **Linke**: (*float*) Atmospheric turbidity factor (Default: 3.0)
  
Output Parameters
`````````````````

* **Output**: Output raster containing stomatal conductance estimation


.. raw:: html

   <h2>References</h2>

.. [blonquist2009] Blonquist, J. M., Norman, J. M., Bugbee, B., 2009. Automated measurements of canopy stomatal conductance based on infrared temperature. Agricultural and Forest Meteorology, 149, 1931-1945
