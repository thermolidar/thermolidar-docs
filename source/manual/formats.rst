Supported file formats
~~~~~~~~~~~~~~~~~~~~~~

SPD/UPD
```````

The *Sorted Pulse Data* (**SPD**) file format [Bunting2013b]_ has been designed specifically for the storage of LiDAR waveform and discrete return data acquired by **TLS**, **ALS** and space borne systems, and includes support for multiple wavelengths within a single file. The SPD format also supports 2D spatial indexing of the pulses, and the *Unsorted Pulse Data* (**UPD**) files are SPD files without a spatial index. 

LAS
```

The LASer (**LAS**) file format supported is aligned with the LAS v1.2 specifications. LAS 1.2 files are exported through the LibLAS_ library so as with the importer only discrete return data are supported. 

ASCII
`````


The ASCII format requires a ``schema`` written in XML to be supplied. The parse expects a single return per line and the resulting pulses will only contain a single return. The following are some examples of schemas for common ASCII formats.

A schema for the PTS format:

.. code-block:: xml
   
   <?xml version="1.0" encoding="UTF-8" ?>
   <line delimiter=" " comment="#" ignorelines="0" >
      <field name="X" type="spd_double" index="0" />
      <field name="Y" type="spd_double" index="1" />
      <field name="Z" type="spd_float" index="2" />
      <field name="AMPLITUDE_RETURN" type="spd_uint" index="3" />
      <field name="RED" type="spd_uint" index="4" />
      <field name="GREEN" type="spd_uint" index="5" />
      <field name="BLUE" type="spd_uint" index="6" />
   </line>

A schema for the XYZ format:

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8" ?>
   <line delimiter="," comment="#" ignorelines="0" >
      <field name="X" type="spd_double" index="0" />
      <field name="Y" type="spd_double" index="1" />
      <field name="Z" type="spd_float" index="2" />
      <field name="AMPLITUDE_RETURN" type="spd_uint" index="3" />
   </line>