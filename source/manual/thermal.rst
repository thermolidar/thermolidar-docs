Thermal Processing
==================
Thermal processing tools data allows a user to perform thermal imaging calibration using the Emissive Empirical Line Method (EELM) which is a common method for airborne thermal data processing based on the ‘In-scene atmospheric correction methods. These approaches were developed to remove atmospheric effects from hyper-spectral imaging data allowing the user to utilize similar conditions to the atmosphere state. The advantage of using this type of methods over model-based methods based in radiative transfer theory is that they capture the true state of the atmosphere at the time of data collection and the relative low computational efforts required to perform the corrections (in comparison with radiative transfer models approaches). The main difficulty for in-scene method is getting correctly the field measurements parameters required for the correction algorithm.

The Emissive Empirical Line Method (EEELM) is the infra-red extension of the widely known Empirical Line Method (ELM) atmospheric correction. EELM employs a linear regression for each band to relate at-sensor radiance with the ground leaving radiance (GLR) via target emissivity and temperature by generating atmospheric transmission, up-welling radiance, and down-welling radiance terms. EELM requires at least one bright target, one dark target and it is also recommended to measure any intermediate target.

Use Empirical Line Compute Factors calibration to force spectral data to match selected field reflectance spectra. A linear regression is used for each band to equate DN and reflectance. This is equivalent to removing the solar irradiance and the atmospheric path radiance. The following equation shows how the empirical line gain and offset values are calculated.

This tool will also allow the user to calculate the difference between Crown Temperature minus Air temperature (Tc-Ta). This indicator has been widely demonstrated to be related with different physiological indicators such as stem water potential, stomatal conductance or sap flow rate. User need to select as input the thermal imaging and the air temperature collected in the same time of the airborne imaging acquisitions.

.. figure:: ../../images/thermal/workflow_thermalproc.png
   :width: 450px
   :figwidth: 700px
   :align: center

Thermal Calibration
-------------------

This module allows to calibrate thermal images based on the calibration data obtained in the field. During the calibration process, spectral data is linearly correlated with selected field reflectance spectral.

.. figure:: ../../images/thermal/T_calibration.jpg
   :width: 450px
   :figwidth: 700px
   :align: center
	
Required Input Parameters
~~~~~~~~~~~~~~~~~~~~~~~~~

* **Temperature layer**: Thermal raster
* **Temperature AOIs**: Shapefile containing information on temperature field measurements
* **Temperature Field**: Vector's field containing temperature

Output Parameters
~~~~~~~~~~~~~~~~~
	
* **Output layer**: Calibrated thermal raster output.
	
Tc - Ta
-------

This module normalizes the temperature raster image according to the air temperature during the image acquisition.

.. figure:: ../../images/thermal/Tc_Ta.jpg
   :width: 450px
   :figwidth: 700px
   :align: center
	
Required Input Parameters
~~~~~~~~~~~~~~~~~~~~~~~~~

* **Input layer**: Thermal raster
* **Air temperature**: Constant air temperature measure at flight time

Output Parameters
~~~~~~~~~~~~~~~~~
	
* **Output layer**: Thermal raster output including normalized temperature.