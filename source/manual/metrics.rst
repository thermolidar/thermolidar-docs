How to define metrics?
~~~~~~~~~~~~~~~~~~~~~~
In order to create a valid XML file containing metrics, some specifications are required. Within the **metrics** tags a list of metrics can be provided by the metric tag. Within each metric the **field** attribute is used to name the raster band or vector attribute. Here is a template of a valid metric XML file:

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8" ?>
   <!--
     Description:
       XML File for execution within SPDLib
       This file contains a template for the
       metrics XML interface.

     Created by Roberto Antolin on Thu Apr 24 16:33:36 2014.
   -->
    
   <spdlib:metrics xmlns:spdlib="http://www.spdlib.org/xml/">
     <!-- List metrics here -->    
   </spdlib:metrics>  


Each metric can be restricted to a given range of values (for range, amplitude, topographic Z and height), return ID and classification. Note that some of this values might or might be not stored in LiDAR files, but it highly depends on what the contractor had supplied. However, it will be possible to assess height metrics since **height** is a value provided by the :ref:`spddefheight` module. 

Here below there is a sample of list of metrics:

* General metrics
   * numpulses
   * canopycover
   * canopycoverpercent

* Height metrics:
   * hscoi
   * numreturnsheight
   * sumheight
   * meanheight
   * medianheight
   * modeheight
   * minheight
   * maxheight
   * dominantheight
   * stddevheight
   * varianceheight
   * absdeviationheight
   * coefficientofvariationheight
   * percentileheight
   * skewnessheight
   * personmodeheight
   * personmedianheight
   * kurtosisheight
   * returnsaboveheightmetric
   * returnsbelowheightmetric
   * canopycoverheight
   * weibullalphaheight
   * weibullbetaheight
   * weibullquantileheight
   * percent

A complete list of metrics and its parameters can be found :doc:`here <allmetrics>`.

Operators
~~~~~~~~~
Mathematical operators can also be applied to either other metrics or operators, allowing a wider range of LiDAR metrics to be derived. The list of available operators and their XML interface is shown below:

.. code-block:: xml

   <!-- Operator Metric Tags -->
   <spdlib:metric metric="add" field="Out_Name" >
      <!--  At least two metrics -->
   </spdlib:metric>
   <spdlib:metric metric="minus" field="Out_Name" >
      <!--  Requires two metrics -->
   </spdlib:metric>
   <spdlib:metric metric="multiply" field="Out_Name" >
      <!--  At least two metrics -->
   </spdlib:metric>
   <spdlib:metric metric="divide" field="Out_Name" >
      <!--  Requires two metrics -->
   </spdlib:metric>
   <spdlib:metric metric="pow" field="Out_Name" >
      <!--  Requires two metrics -->
   </spdlib:metric>
   <spdlib:metric metric="abs" field="Out_Name" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="sqrt" field="Out_Name" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="sine" field="Out_Name" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="cosine" field="Out_Name" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="tangent" field="Out_Name" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="invsine" field="Out_Name" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="invcos" field="Out_Name" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="invtan" field="Out_Name" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="log10" field="Out_Name" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="ln" field="Out_Name" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="exp" field="Out_Name" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="percentage" field="Out_Name" >
      <!--  Requires two metrics -->
   </spdlib:metric>
   <spdlib:metric metric="addconst" field="Out_Name" const="double" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="minusconstfrom" field="Out_Name" const="double" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="minusfromconst" field="Out_Name" const="double" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="multiplyconst" field="Out_Name" const="double" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="dividebyconst" field="Out_Name" const="double" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="divideconstby" field="Out_Name" const="double" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="powmetricconst" field="Out_Name" const="double" >
      <!--  Requires one metric -->
   </spdlib:metric>
   <spdlib:metric metric="powconstmetric" field="Out_Name" const="double" >
      <!--  Requires one metric -->
   </spdlib:metric>

Here below is an example of an SPD metrics XML file template containing the percentage of not-ground returns --as a mathematical operation of the number of not-ground returns and the total number of returns-- maximum height, the average and median heights, the canopy cover and the height 95th percentile:

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8" ?>
   <!--
     Description:
       XML File for execution within SPDLib
       This file contains a template for the
       metrics XML interface.

     Created by Roberto Antolin on Thu Apr 24 16:33:36 2014.
   -->
    
   <spdlib:metrics xmlns:spdlib="http://www.spdlib.org/xml/" />
     <spdlib:metric metric="percentage" field="CoverRts" />
       <spdlib:metric metric="numreturnsheight" field="Out_Name" return="All" class="NotGrd" lowthreshold="0.2" />
       <spdlib:metric metric="numreturnsheight" field="Out_Name" return="All" class="All" />
     </spdlib:metric>
     <spdlib:metric metric="maxheight" field="MaxH" return="All" class="NotGrd" lowthreshold="0.2" />
     <spdlib:metric metric="meanheight" field="MeanH" return="All" class="NotGrd" lowthreshold="0.2" />
     <spdlib:metric metric="medianheight" field="MedianH" return="All" class="NotGrd" lowthreshold="0.2" />
     <spdlib:metric metric="percentileheight" field="95thPerH" percentile="95" return="All" class="NotGrd" lowthreshold="0.2" />
   </spdlib:metrics>
